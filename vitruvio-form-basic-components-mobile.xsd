<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">

	<!-- 
	
	Componentes basicos para uso nos formularios do Vitruvio

 -->

	<!-- Parametros comuns nos Campos -->
	<xs:include schemaLocation="vitruvio-form-common-attrs.xsd" />
	<xs:include schemaLocation="vitruvio-form-field-validators.xsd" />
	<xs:include schemaLocation="vitruvio-form-field-events.xsd" />

	<!-- Container de dados para campos de selecao com entradas predefinidas -->
	<xs:complexType name="entry">
		<xs:attribute name="key" type="xs:string" />
		<xs:attribute name="value" type="xs:string" use="required" />
	</xs:complexType>

	<!-- Definicao de elementos para campos -->
	<xs:complexType name="Label">
		<xs:choice>
			<xs:element name="value" />
			<xs:element name="formValue">
				<xs:complexType>
					<xs:simpleContent>
						<xs:extension base="xs:string">
							<xs:attribute name="valueWhenNull" type="xs:string" />
						</xs:extension>
					</xs:simpleContent>
				</xs:complexType>
			</xs:element>
		</xs:choice>
		<xs:attribute name="id" type="xs:string" use="required" />
		<xs:attribute name="visible" type="xs:boolean" />
		<xs:attribute name="width" type="xs:string" />
		<xs:attribute name="contentMode" default="PLAIN_TEXT">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:enumeration value="PLAIN_TEXT" />
					<xs:enumeration value="PRE_FORMATED" />
					<xs:enumeration value="HTML" />
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="expandRatio" type="xs:decimal" />
	</xs:complexType>

	<xs:complexType name="TextField">
		<xs:sequence minOccurs="0" maxOccurs="unbounded">
			<xs:element name="validation" type="fieldValidators" minOccurs="0" maxOccurs="1" />
			<xs:element name="events" type="textFieldEvents" minOccurs="0" maxOccurs="1" />
		</xs:sequence>
		<xs:attributeGroup ref="mobileStringFieldAttrs" />
		<xs:attributeGroup ref="sizeBasedFieldAttrs" />
		<xs:attributeGroup ref="mobileTextFieldAttrs" />
	</xs:complexType>

	<xs:complexType name="PasswordField">
		<xs:sequence minOccurs="0" maxOccurs="unbounded">
			<xs:element name="validation" type="fieldValidators" minOccurs="0" maxOccurs="1" />
			<xs:element name="events" type="textFieldEvents" minOccurs="0" maxOccurs="1" />
		</xs:sequence>
		<xs:attributeGroup ref="mobileStringFieldAttrs" />
		<xs:attributeGroup ref="sizeBasedFieldAttrs" />
		<xs:attributeGroup ref="mobileTextFieldAttrs" />
	</xs:complexType>

	<xs:complexType name="TextArea">
		<xs:sequence minOccurs="0" maxOccurs="unbounded">
			<xs:element name="validation" type="fieldValidators" minOccurs="0" maxOccurs="1" />
			<xs:element name="events" type="textFieldEvents" minOccurs="0" maxOccurs="1" />
		</xs:sequence>
		<xs:attributeGroup ref="mobileStringFieldAttrs" />
		<xs:attributeGroup ref="sizeBasedFieldAttrs" />
		<xs:attributeGroup ref="mobileTextFieldAttrs" />
		<xs:attribute name="rows" type="xs:positiveInteger" />
	</xs:complexType>

	<xs:complexType name="NumericField">
		<xs:sequence minOccurs="0" maxOccurs="unbounded">
			<xs:element name="validation" type="scriptValidators" minOccurs="0" maxOccurs="1" />
			<xs:element name="events" type="textFieldEvents" minOccurs="0" maxOccurs="1" />
		</xs:sequence>
		<xs:attributeGroup ref="mobileStringFieldAttrs" />
		<xs:attributeGroup ref="sizeBasedFieldAttrs" />
		<xs:attributeGroup ref="mobileTextFieldAttrs" />
		<xs:attributeGroup ref="numericTextFieldAttrs" />
	</xs:complexType>

	<xs:complexType name="DecimalField">
		<xs:sequence minOccurs="0" maxOccurs="unbounded">
			<xs:element name="validation" type="scriptValidators" minOccurs="0" maxOccurs="1" />
			<xs:element name="events" type="textFieldEvents" minOccurs="0" maxOccurs="1" />
		</xs:sequence>
		<xs:attributeGroup ref="fieldAttrs" />
		<xs:attributeGroup ref="sizeBasedFieldAttrs" />
		<xs:attributeGroup ref="textFieldAttrs" />
		<xs:attributeGroup ref="numericTextFieldAttrs" />
		<xs:attribute name="format" type="xs:string" />
		<xs:attribute name="decimalSeparator" type="xs:string" />
		<xs:attribute name="groupingSeparator" type="xs:string" />
		<xs:attribute name="selectTextOnFocus" type="xs:boolean" />
		<xs:attribute name="currencyField" type="xs:boolean" />
	</xs:complexType>

	<xs:complexType name="MaskedField">
		<xs:sequence minOccurs="0" maxOccurs="unbounded">
			<xs:choice>
				<xs:element name="prefixed-masks">
					<xs:complexType>
						<xs:sequence minOccurs="0" maxOccurs="unbounded">
							<xs:element name="value">
								<xs:complexType>
									<xs:attribute name="prefix" type="xs:string" use="required" />
									<xs:attribute name="mask" type="xs:string" use="required" />
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:element name="validation" type="scriptValidators" minOccurs="0" maxOccurs="unbounded" />
				<xs:element name="events" type="textFieldEvents" minOccurs="0" maxOccurs="1" />
			</xs:choice>
		</xs:sequence>
		<xs:attributeGroup ref="mobileStringFieldAttrs" />
		<xs:attributeGroup ref="sizeBasedFieldAttrs" />
		<xs:attributeGroup ref="mobileTextFieldAttrs" />
		<xs:attribute name="mask" type="xs:string" use="required" />
	</xs:complexType>

	<xs:complexType name="DateField">
		<xs:sequence minOccurs="0" maxOccurs="1">
			<xs:element name="validation" type="scriptValidators" minOccurs="0" maxOccurs="1" />
			<xs:element name="events" type="allEvents" minOccurs="0" maxOccurs="1" />
		</xs:sequence>
		<xs:attributeGroup ref="fieldAttrs" />
		<xs:attribute name="format" type="xs:string" />
		<xs:attribute name="resolution">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:enumeration value="MINUTE" />
					<xs:enumeration value="HOUR" />
					<xs:enumeration value="DAY" />
					<xs:enumeration value="MONTH" />
					<xs:enumeration value="YEAR" />
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
	</xs:complexType>

	<xs:complexType name="ComboBox">
		<xs:sequence>
			<xs:element name="entry" type="entry" minOccurs="1" maxOccurs="unbounded" />
			<xs:element name="validation" type="scriptValidators" minOccurs="0" maxOccurs="1" />
			<xs:element name="events" type="allEvents" minOccurs="0" maxOccurs="1" />
		</xs:sequence>
		<xs:attributeGroup ref="fieldAttrs" />
		<xs:attributeGroup ref="selectFieldAttr" />
	</xs:complexType>

	<xs:complexType name="ListSelect">
		<xs:sequence>
			<xs:element name="entry" type="entry" minOccurs="1" maxOccurs="unbounded" />
			<xs:element name="validation" type="scriptValidators" minOccurs="0" maxOccurs="1" />
			<xs:element name="events" type="allEvents" minOccurs="0" maxOccurs="1" />
		</xs:sequence>
		<xs:attributeGroup ref="fieldAttrs" />
		<xs:attributeGroup ref="selectFieldAttr" />
		<xs:attribute name="rows" type="xs:positiveInteger" />
	</xs:complexType>

	<xs:complexType name="OptionGroup">
		<xs:sequence>
			<xs:element name="entry" type="entry" minOccurs="1" maxOccurs="unbounded" />
			<xs:element name="validation" type="scriptValidators" minOccurs="0" maxOccurs="1" />
			<xs:element name="events" type="valueEvents" minOccurs="0" maxOccurs="1" />
		</xs:sequence>
		<xs:attributeGroup ref="mobileFieldAttrs" />
		<xs:attributeGroup ref="selectFieldAttr" />
		<xs:attribute name="disposition" default="horizontal">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:enumeration value="horizontal" />
					<xs:enumeration value="vertical" />
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
	</xs:complexType>

	<xs:complexType name="CheckBox">
		<xs:sequence minOccurs="0" maxOccurs="1">
			<xs:element name="valueOverride" minOccurs="0" maxOccurs="1">
				<xs:complexType>
					<xs:attribute name="forTrue" type="xs:string" use="required" />
					<xs:attribute name="forFalse" type="xs:string" use="required" />
				</xs:complexType>
			</xs:element>
			<xs:element name="events" type="valueEvents" minOccurs="0" maxOccurs="1" />
		</xs:sequence>
		<xs:attributeGroup ref="mobileBasicFieldAttrs" />
		<xs:attribute name="expandRatio" type="xs:decimal" />
		<xs:attribute name="type" use="required">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:enumeration value="boolean" />
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
	</xs:complexType>

	<xs:complexType name="RatingStarsCaptions">
		<xs:attribute name="value" type="xs:integer" use="required" />
		<xs:attribute name="caption" type="xs:string" use="required" />
	</xs:complexType>

	<xs:complexType name="RatingStars">
		<xs:sequence minOccurs="0" maxOccurs="unbounded">
			<xs:element name="captions">
				<xs:complexType>
					<xs:sequence minOccurs="1" maxOccurs="unbounded">
						<xs:element name="caption" type="RatingStarsCaptions" />
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="validation" type="fieldValidators" minOccurs="0" maxOccurs="1" />
			<xs:element name="events" type="valueEvents" minOccurs="0" maxOccurs="1" />
		</xs:sequence>
		<xs:attributeGroup ref="decimalTypefieldAttrs" />
		<xs:attribute name="maxValue" type="xs:positiveInteger" />
		<xs:attribute name="animated" type="xs:boolean" />
		<xs:attribute name="setProcessRating" type="xs:boolean" />
	</xs:complexType>

	<xs:complexType name="Toggle">
		<xs:sequence minOccurs="0" maxOccurs="1">
			<xs:element name="events" type="valueEvents" minOccurs="0" maxOccurs="1" />
		</xs:sequence>
		<xs:attributeGroup ref="mobileBasicFieldAttrs" />
		<xs:attribute name="expandRatio" type="xs:decimal" />
		<xs:attribute name="type" use="required">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:enumeration value="boolean" />
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
	</xs:complexType>

	<xs:complexType name="Barcode">
		<xs:sequence minOccurs="0" maxOccurs="1">
			<xs:element name="events" type="allEvents" minOccurs="0" maxOccurs="1" />
		</xs:sequence>
		<xs:attributeGroup ref="mobileBasicFieldAttrs" />
		<xs:attribute name="expandRatio" type="xs:decimal" />
		<xs:attribute name="type" use="required">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:enumeration value="string" />
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="formats" type="xs:string" />
		<xs:attribute name="prefix" type="xs:string" />
		<xs:attribute name="suffix" type="xs:string" />
		<xs:attribute name="titleField" type="xs:string" />
		<xs:attribute name="descriptionField" type="xs:string" />
		<xs:attribute name="allowElevationForGroups" type="xs:string" />
	</xs:complexType>

</xs:schema>