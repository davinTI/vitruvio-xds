<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">

<!-- Parametros comuns nos Campos -->
<xs:include schemaLocation="vitruvio-form-common-attrs.xsd" />

<!-- Componentes padrao -->
<xs:include schemaLocation="vitruvio-form-basic-components-mobile.xsd" />

<!-- Componentes para uso com bancos de dados -->
<xs:include schemaLocation="vitruvio-form-db-components.xsd" />

<!-- Componentes especificos ao contexto da aplicacao -->
<xs:include schemaLocation="vitruvio-form-specific-components.xsd" />

<!-- Componentes flexiveis, orientados a script -->
<xs:include schemaLocation="vitruvio-form-script-components.xsd" />

<!-- Suporte a Widgets -->
<xs:include schemaLocation="vitruvio-form-widgets.xsd" />

<!-- Parametros comuns nos layouts ordenados -->
<xs:attributeGroup name="layoutMandatoryAttr">
	<xs:attribute name="id" type="xs:string" />
	<xs:attribute name="isLayout" fixed="true" />
	<xs:attribute name="expandRatio" type="xs:decimal" />
	<xs:attribute name="caption" use="optional" type="xs:string" />
	<xs:attribute name="backgroundColor" use="optional" type="xs:string" />
	<xs:attributeGroup ref="elementAlign" /> 
</xs:attributeGroup>

<xs:attributeGroup name="orderedLayoutAttr">
	<xs:attribute name="margin" type="xs:boolean" default="false" />
	<xs:attribute name="spacing" type="xs:boolean" default="false" />
	<xs:attribute name="width" type="xs:string" />
	<xs:attribute name="height" type="xs:string" />
	<xs:attribute name="visible" type="xs:boolean" />
</xs:attributeGroup>

<!-- Grupo de campos -->
<xs:group name="fieldGroup">
	<xs:choice>
		<!-- Components basicos -->
			<xs:element name="Label" type="Label"  />
			<xs:element name="TextField" type="TextField" />
			<xs:element name="PasswordField" type="PasswordField" />
			<xs:element name="TextArea" type="TextArea" />
			<xs:element name="NumericField" type="NumericField" />
			<xs:element name="DecimalField" type="DecimalField" />
			<xs:element name="MaskedField" type="MaskedField" />
			<xs:element name="DateField" type="DateField" />
			<xs:element name="ComboBox" type="ComboBox" />
			<xs:element name="ListSelect" type="ListSelect" />
			<xs:element name="OptionGroup" type="OptionGroup" />
			<xs:element name="CheckBox" type="CheckBox" />
			<xs:element name="RatingStars" type="RatingStars" />
			<xs:element name="GoogleMapsField" type="GoogleMapsField" />
			
			<!-- Componentes DB -->
			<xs:element name="DBComboBox" type="DBComboBox" />
			<xs:element name="DBTable" type="DBTable" />
			<xs:element name="DBSearchField" type="DBSearchField" />
			<xs:element name="DBTwinColSelect" type="DBTwinColSelect" />
			<xs:element name="DBPropertyField" type="DBPropertyField" />
			
			<!-- Componentes Especificos -->
			<xs:element name="UserField" type="UserField" />
			<xs:element name="UserComboBox" type="UserComboBox" />
			<xs:element name="RoleComboBox" type="RoleComboBox" />
			<xs:element name="UserPropertyField" type="UserPropertyField" />
			<xs:element name="ProcessInstanceIdField" type="ProcessInstanceIdField" />
			<xs:element name="ContextDateTimeField" type="ContextDateTimeField" />
			<xs:element name="MarcadorField" type="MarcadorField" />
			<xs:element name="UserMultiSelect" type="UserMultiSelect" />
			<xs:element name="RoleMultiSelect" type="RoleMultiSelect" />
			
			
			<!-- Componentes via Script -->
			<xs:element name="ScriptComboBox" type="ScriptComboBox" />
			
	</xs:choice>
</xs:group>

<!-- Grupo de layouts -->
<xs:group name="layoutGroup">
	<xs:choice>
		<!-- Elementos visuais e containers -->
			<xs:element name="VerticalLayout" type="VerticalLayout" />
			<xs:element name="HorizontalLayout" type="HorizontalLayout" />
			<xs:element name="Panel" type="Panel" />
			<xs:element name="ScrollPanel" type="ScrollPanel" />
			<xs:element name="TabLayout" type="TabLayout" />
			<xs:element name="LayoutGroup" type="LayoutGroup" />
			<xs:element name="ComplexPanel" type="ComplexPanel" />
			<xs:element name="AbsoluteLayout" type="AbsoluteLayout" />
			<xs:element name="WindowLayout" type="WindowLayout" />
			<xs:element name="CrudPanel" type="CrudPanel" />
	</xs:choice>
</xs:group>

<!-- Definicao de elementos possiveis por padrao num container de componentes -->
<xs:group name="componentContainer">
	<xs:choice>
			<!-- Layouts -->
			<xs:group ref="layoutGroup" />
			<xs:element name="PopupButtonLayout" type="PopupButtonLayout" />
			
			<!-- Componentes reutilizaveis da biblioteca -->
			<xs:element name="ComplexComponent" type="LibraryComplexComponent" />
			<xs:element name="FieldComponent" type="LibraryFieldComponent" />
			
			<!-- Suporte a definicao de widgets -->
			<xs:group ref="CoreWidgets" />
			<xs:element name="CustomWidget" type="CustomWidget" />
			
			<!-- Campos -->
			<xs:group ref="fieldGroup" />
	</xs:choice>
</xs:group>

<!-- Layouts -->
<xs:complexType name="VerticalLayout">
	<xs:sequence>
		<xs:group ref="componentContainer" minOccurs="0" maxOccurs="unbounded"/>
	</xs:sequence>
	<xs:attributeGroup ref="layoutMandatoryAttr"/>
	<xs:attributeGroup ref="orderedLayoutAttr"/>
</xs:complexType>

<xs:complexType name="HorizontalLayout">
	<xs:sequence>
		<xs:group ref="componentContainer" minOccurs="0" maxOccurs="unbounded"/>
	</xs:sequence>
	<xs:attributeGroup ref="layoutMandatoryAttr"/>
	<xs:attributeGroup ref="orderedLayoutAttr"/>
	<xs:attribute name="wrapping" type="xs:boolean" />
</xs:complexType>

<!-- Tabs -->
<xs:complexType name="TabComponent">
	<xs:sequence>
		<xs:group ref="componentContainer" minOccurs="1" maxOccurs="1"/>
	</xs:sequence>
	<xs:attribute name="caption" type="xs:string" use="required" />
	<xs:attribute name="visible" type="xs:boolean" />
</xs:complexType>
<xs:complexType name="TabLayout">
	<xs:sequence>
		<xs:element name="TabChangeScript" type="script" minOccurs="0" maxOccurs="1" />
		<xs:element name="Tab" type="TabComponent" minOccurs="1" maxOccurs="unbounded" />
	</xs:sequence>
	<xs:attribute name="framed" type="xs:boolean" />
	<xs:attribute name="compact" type="xs:boolean" />
	<xs:attribute name="centerTabs" type="xs:boolean" />
	<xs:attribute name="width" type="xs:string" />
	<xs:attribute name="height" type="xs:string" />	
	<xs:attributeGroup ref="layoutMandatoryAttr"/>
</xs:complexType>

<xs:complexType name="LayoutGroup">
	<xs:choice minOccurs="1" maxOccurs="1">
		<xs:element name="VerticalLayout" type="VerticalLayout" />
		<xs:element name="HorizontalLayout" type="HorizontalLayout" />
	</xs:choice>
	<xs:attribute name="caption" type="xs:string" />
	<xs:attribute name="width" type="xs:string" />
	<xs:attribute name="height" type="xs:string" />
</xs:complexType>

<xs:complexType name="BasePanel">
	<xs:attribute name="id" type="xs:string" />
	<xs:attribute name="caption" type="xs:string" />
	<xs:attribute name="width" type="xs:string" />
	<xs:attribute name="height" type="xs:string" />
	<xs:attribute name="margin" type="xs:boolean" />
	<xs:attribute name="expandRatio" type="xs:decimal" />
	<xs:attribute name="backgroundColor" type="xs:string" use="optional" />
	<xs:attribute name="visible" type="xs:boolean" />
	<xs:attributeGroup ref="elementAlign" />
</xs:complexType>

<xs:complexType name="Panel">
	<xs:complexContent>
		<xs:extension base="BasePanel">
			<xs:choice minOccurs="1" maxOccurs="1">
				<xs:element name="VerticalLayout" type="VerticalLayout" />
				<xs:element name="HorizontalLayout" type="HorizontalLayout" />
				<xs:element name="AbsoluteLayout" type="AbsoluteLayout" />
				<xs:element name="TabLayout" type="TabLayout" />
			</xs:choice>
		</xs:extension>
	</xs:complexContent>
</xs:complexType>

<xs:complexType name="ScrollPanel">
	<xs:choice minOccurs="1" maxOccurs="1">
		<xs:element name="VerticalLayout" type="VerticalLayout" />
		<xs:element name="HorizontalLayout" type="HorizontalLayout" />
		<xs:element name="AbsoluteLayout" type="AbsoluteLayout" />
		<xs:element name="TabLayout" type="TabLayout" />
	</xs:choice>
	<xs:attribute name="id" type="xs:string" />
	<xs:attribute name="width" type="xs:string" />
	<xs:attribute name="height" type="xs:string" />
	<xs:attribute name="expandRatio" type="xs:decimal" />
	<xs:attribute name="visible" type="xs:boolean" />
	<xs:attribute name="backgroundColor" type="xs:string" use="optional" />
</xs:complexType>

<xs:complexType name="WindowLayout">
	<xs:sequence>
		<xs:choice minOccurs="1" maxOccurs="1">
			<xs:element name="VerticalLayout" type="VerticalLayout" />
			<xs:element name="HorizontalLayout" type="HorizontalLayout" />
			<xs:element name="AbsoluteLayout" type="AbsoluteLayout" />
			<xs:element name="TabLayout" type="TabLayout" />
		</xs:choice>
		<xs:element name="Config" minOccurs="0" maxOccurs="1">
			<xs:complexType>
				<xs:sequence>
					<xs:element name="ControlButton" minOccurs="0" maxOccurs="1">
						<xs:complexType>
							<xs:sequence>
								<xs:element name="onClickScript" type="script" minOccurs="1" maxOccurs="1" />
							</xs:sequence>
							<xs:attribute name="description" type="xs:string" />
							<xs:attributeGroup ref="basicElement" />
							<xs:attributeGroup ref="formButtonAttrs" />
						</xs:complexType>
					</xs:element>
					<xs:element name="onChangeVisibilityScript" type="script" minOccurs="0" maxOccurs="1" />
				</xs:sequence>
			</xs:complexType>
		</xs:element>
	</xs:sequence>
	
	<xs:attribute name="windowWidth" type="xs:string" />
	<xs:attribute name="windowHeight" type="xs:string" />
	<xs:attribute name="windowTitle" type="xs:string" />
	<xs:attribute name="windowClosable" type="xs:boolean" />
	<xs:attribute name="windowResizable" type="xs:boolean" />
	<xs:attribute name="windowModal" type="xs:boolean" />
	
	<xs:attributeGroup ref="layoutMandatoryAttr"/>
</xs:complexType>

<xs:complexType name="ComplexPanelActionItem">
	<xs:sequence>
		<xs:element name="clickScript" type="script" minOccurs="1" maxOccurs="1" />
	</xs:sequence>
	<xs:attribute name="caption" type="xs:string" />
	<xs:attribute name="description" type="xs:string" />
	<xs:attributeGroup ref="font-awesome-icons" />
</xs:complexType>

<xs:complexType name="CrudSession">
	<xs:sequence> 
		<xs:element name="Filter" type="CrudSessionFilter" minOccurs="0" maxOccurs="1">
		</xs:element>
		<xs:choice minOccurs="1" maxOccurs="1">
			<xs:element name="VerticalLayout" type="VerticalLayout" />
			<xs:element name="HorizontalLayout" type="HorizontalLayout" />
		</xs:choice>
	</xs:sequence>
	<xs:attribute name="caption" type="xs:string" />
	<xs:attribute name="subCaption" type="xs:string" />
</xs:complexType>

<xs:complexType name="CrudSessionFilter">
	<xs:choice minOccurs="1" maxOccurs="1">
		<xs:element name="VerticalLayout" type="VerticalLayout" />
		<xs:element name="HorizontalLayout" type="HorizontalLayout" />
	</xs:choice>
</xs:complexType>

<xs:complexType name="CrudPanel">
		<xs:sequence> 
			<xs:element name="Header" minOccurs="0" maxOccurs="1">
				<xs:complexType>
					<xs:sequence>
						<xs:choice maxOccurs="1" minOccurs="0">
							<xs:element name="Caption"/>
							<xs:element name="htmlCaption" />
						</xs:choice>
						<xs:choice maxOccurs="1" minOccurs="0">
							<xs:element name="SubCaption"/>
						</xs:choice>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="Section" type="CrudSession" minOccurs="1" maxOccurs="unbounded" />
		 </xs:sequence>
		 <xs:attributeGroup ref="orderedLayoutAttr"></xs:attributeGroup>
		  <xs:attributeGroup ref="layoutMandatoryAttr"></xs:attributeGroup>
</xs:complexType>

<xs:complexType name="ComplexPanel">
	<xs:complexContent>
		<xs:extension base="BasePanel">
			<xs:sequence>
				<xs:element name="Config" minOccurs="0" maxOccurs="1">
					<xs:complexType>
						<xs:sequence>
							<xs:choice maxOccurs="1" minOccurs="0">
								<xs:element name="caption"/>
								<xs:element name="htmlCaption" />
							</xs:choice>
							<xs:element name="buttons" minOccurs="0" maxOccurs="1">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="button" type="ComplexPanelActionItem" minOccurs="1" maxOccurs="unbounded" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="menu" minOccurs="0" maxOccurs="1">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="item" type="ComplexPanelActionItem" minOccurs="1" maxOccurs="unbounded" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
				<xs:choice minOccurs="1" maxOccurs="1">
					<xs:element name="VerticalLayout" type="VerticalLayout" />
					<xs:element name="HorizontalLayout" type="HorizontalLayout" />
				</xs:choice>
			</xs:sequence>
			<xs:attribute name="captionSize" use="optional" >
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="LIGHT" />
						<xs:enumeration value="H4" />
						<xs:enumeration value="H3" />
						<xs:enumeration value="H2" />
						<xs:enumeration value="H1" />
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
		</xs:extension>
	</xs:complexContent>
</xs:complexType>

<!-- Poupup Layout -->
<xs:complexType name="PopupButtonLayoutFilterTemplate">
	<xs:sequence>
		<xs:element name="SearchButton" type="ButtonWidget" minOccurs="1" maxOccurs="1" />
	</xs:sequence>
	<xs:attribute name="renderCloseButton" type="xs:boolean" />
	<xs:attribute name="keepDefaultButtonStyle" type="xs:boolean" />
</xs:complexType>
<xs:complexType name="PopupButtonLayoutEvents">
	<xs:all>
		<xs:element name="onPopupShow" type="script" minOccurs="0" maxOccurs="1" />
		<xs:element name="onPopupHide" type="script" minOccurs="0" maxOccurs="1" />
	</xs:all>
</xs:complexType>

<xs:complexType name="PopupButtonBaseLayout">
	<xs:attribute name="width" type="xs:string" />
	<xs:attribute name="height" type="xs:string" />
	<xs:attribute name="visible" type="xs:boolean" />
	<xs:attribute name="description" type="xs:string" />
	<xs:attribute name="closePopupOnOutsideClick" type="xs:boolean" />
	<xs:attributeGroup ref="layoutMandatoryAttr"/>
</xs:complexType>

<xs:complexType name="PopupButtonLayout">
	<xs:complexContent>
		<xs:extension base="PopupButtonBaseLayout">
			<xs:sequence>
				<xs:choice minOccurs="1" maxOccurs="1">
					<xs:element name="VerticalLayout" type="VerticalLayout" />
					<xs:element name="HorizontalLayout" type="HorizontalLayout" />
					<xs:element name="AbsoluteLayout" type="AbsoluteLayout" />
					<xs:element name="TabLayout" type="TabLayout" />
				</xs:choice>
				<xs:element name="Template" minOccurs="0" maxOccurs="1">
					<xs:complexType>
						<xs:choice>
							<xs:element name="Filter" type="PopupButtonLayoutFilterTemplate" />
						</xs:choice>
					</xs:complexType>
				</xs:element>
				<xs:element name="events" type="PopupButtonLayoutEvents" minOccurs="0" maxOccurs="1" />
			</xs:sequence>
		</xs:extension>
	</xs:complexContent>
</xs:complexType>

<xs:complexType name="AbsoluteBaseLayout">
	<xs:attribute name="dragAndDropEnabled" type="xs:boolean" />
	<xs:attribute name="width" type="xs:string" />
	<xs:attribute name="height" type="xs:string" />
	<xs:attributeGroup ref="layoutMandatoryAttr"/>
</xs:complexType>

<xs:complexType name="AbsoluteLayout">
	<xs:complexContent>
		<xs:extension base="AbsoluteBaseLayout">
			<xs:sequence>
				<xs:group ref="componentContainer" minOccurs="0" maxOccurs="unbounded"/>
			</xs:sequence>
		</xs:extension>
	</xs:complexContent>
</xs:complexType>

<!-- BIBLIOTECA DE ELEMENTOS -->

<!-- Definicao de um componente da biblioteca -->
<xs:complexType name="complex-component">
	<xs:sequence>
<!-- 		<xs:group ref="layoutGroup" minOccurs="0" maxOccurs="1"/> -->
		<xs:any processContents="skip" minOccurs="1" maxOccurs="1" />
	</xs:sequence>
	<xs:attribute name="id" type="xs:string" use="required"/>
</xs:complexType>

<xs:complexType name="field-component">
	<xs:sequence>
		<xs:group ref="fieldGroup" minOccurs="0" maxOccurs="1"/>
	</xs:sequence>
	<xs:attribute name="id" type="xs:string" use="required"/>
</xs:complexType>

<!-- Definicao de uma biblitoeca de componentes, para reuso na aplicacao -->
<xs:complexType name="library">
	<xs:sequence>
		<xs:element name="complex-component" type="complex-component" minOccurs="0" maxOccurs="unbounded"/>
		<xs:element name="field-component" type="field-component" minOccurs="0" maxOccurs="unbounded"/>
	</xs:sequence>
</xs:complexType>

<!-- Elemento pare referenciar um item complexo da biblioteca -->
<xs:complexType name="LibraryComplexComponent">
	<xs:attribute name="ref" type="xs:string" use="required" />
</xs:complexType>

<!-- Elemento para referenciar um campo especifico da biblioteca, com a possibilidade de sobrescrever seus atributos -->
<xs:complexType name="LibraryFieldComponent">
	<xs:attribute name="ref" type="xs:string" use="required" />
	<xs:anyAttribute processContents="skip" />
</xs:complexType>

<!-- Elementos para lidar com anexos dos processos -->
<xs:complexType name="FormAttachments">
	<xs:sequence minOccurs="0" maxOccurs="unbounded">
		<xs:element name="attachment">
			<xs:complexType>
				<xs:sequence minOccurs="0" maxOccurs="1">
					<xs:element name="allowed-mimetypes">
						<xs:complexType>
							<xs:sequence minOccurs="1" maxOccurs="unbounded">
								<xs:element name="value" type="xs:string" />
							</xs:sequence>
						</xs:complexType>
					</xs:element>
				</xs:sequence>
				<xs:attribute name="id" type="xs:string" use="required" />
				<xs:attribute name="name" type="xs:string" use="required" />
				<xs:attribute name="description" type="xs:string" use="required" />
				<xs:attribute name="required" type="xs:boolean" />
			</xs:complexType>
		</xs:element>
	</xs:sequence>
	<xs:attribute name="allowUndefinedAttachments" type="xs:boolean" />
</xs:complexType>
			
</xs:schema>
